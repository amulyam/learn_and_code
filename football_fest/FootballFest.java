package learn.code;

import java.util.Scanner;

public class FootballFest {
	
	public static void main(String args[] ){
		printFinalPlayer();
	}
	
  	public static void printFinalPlayer(){
        
  		Scanner input = new Scanner(System.in);
        int testCase, playerId, N, presentPlayer, previousPlayer; 
		testCase = input.nextInt();
           
        for (int i = 0; i < testCase; i++) {
            N = input.nextInt();
            Stack myStack = new Stack();
            myStack.push(input.nextInt());
            
            for (int j = 0; j < N; j++) {
            String action = input.next();
            if ("P".equals(action)) {
              playerId = input.nextInt();
              myStack.push(playerId);
            }
            else {
              presentPlayer = myStack.pop();
              previousPlayer = myStack.peek();
              myStack.push(presentPlayer);
              myStack.push(previousPlayer);
            }
            }
            System.out.println("Player "+myStack.peek());
            myStack=null;
        }
     input.close();   
    }

}
