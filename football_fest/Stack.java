package learn.code;

public class Stack {

		private final static int SIZE = 2 * (int) Math.pow(10, 5);
		private int[] stackItems = new int[SIZE];
		private int top = -1;

		public int pop() {
			return stackItems[top--];
		}

		public void push(Integer arrayNum) {
			top++;
			stackItems[top] = arrayNum;
		}

		public int peek() {
			return stackItems[top];
		}

		public Boolean isEmpty() {
			return (top < 0);
		}

		public int size() {
			return top;
		}
	}
