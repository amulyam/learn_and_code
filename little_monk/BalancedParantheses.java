package learn.code;

import java.util.Scanner;

public class BalancedParantheses {

	private int[] stringToIntArray(String[] inputString) {
		int[] a = new int[inputString.length];
		for (int i = 0; i < inputString.length; i++) {
			a[i] = Integer.parseInt(inputString[i]);
		}
		return a;
	}

	private int getMaxBalancedCount(int[] arrayNum) {
		int count = 0;
		int maxCount = 0;
		Stack myStack = new Stack();

		for (int i = 0; i < arrayNum.length; i++) {
			if (myStack.isEmpty() && arrayNum[i] > 0)
				myStack.push(arrayNum[i]);
			else {
				int tempTop = myStack.size();
				if (tempTop >= 0) {
					int peek = myStack.peek();
					if ((peek > arrayNum[i]) && (peek + arrayNum[i] == 0)) {
						count += 2;
						maxCount = (count > maxCount) ? count : maxCount;
						myStack.pop();
					} else {
						myStack.push(arrayNum[i]);
						if (myStack.size() > 0) {
							count = 0;
						}
					}
				}
			}
		}
		return maxCount;
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int maxLimit = 2 * (int) Math.pow(10, 5);
		BalancedParantheses maxLengthObj = new BalancedParantheses();

		int arrayLength = Integer.parseInt(scan.nextLine());
		String[] inputArray = scan.nextLine().split("\\s+");

		if (arrayLength >= 1 && arrayLength <= maxLimit) {
			System.out.println(maxLengthObj.getMaxBalancedCount(maxLengthObj.stringToIntArray(inputArray)));
		}

		else {
			System.out.println("Input should have more than one number");
		}
		scan.close();
	}
}
